
/**
Mappings Util
*/

function trimString(str){
	return (str+"").replace(/^\s+|\s+$/g, '');
}
exports.index = function(req, res){
  res.render('index');
};

exports.programInfo = function(db,url){
return function(req, res) {
        var collection = db.get('programs');
		var url_parts = url.parse(req.url, true);
		var query = url_parts.query;
        
		collection.findOne({
			"_id" : query.id,
		 },function(e,program){
            res.render('programInfo', {
                "program" :program			
            });
        });
    };
}
exports.findProgram = function(req,res) {
    res.render("findProgram");
};
exports.findResults = function(db) {
    return function(req, res) {
	
		var dto =  req.body;

        var title = dto.title;

        var collection = db.get('programs');
        
		collection.find({"title" : title},            
         function (err, results) {
            if (err) {
                //return error
                res.send("There was a problem adding the information to the database.");
            }else if(typeof results == 'undefined' || results.length==0){
				collection.find({ "title": { $regex: title}},
					function (err2, suggestions) {
						if (err2) {
							//return error
							res.send("There was a problem adding the information to the database.");
						}else {
							res.render('findProgram', {
								"query":title,
								"suggestions" : suggestions												
							});
						}
					}
				);
			}else{
				res.render('findProgram', {
					"query":title,
					"results" : results												
				});
			}
		});
	}
}
exports.addProgram = function(db) {    
	return function(req, res) {
        var collection = db.get('programs');
        collection.find({},{},function(e,results){
            res.render('addProgram', {
                "programs" : results
            });
        });
    };
}

exports.addResults = function(db) {
     return function(req, res) {
	
		var dto =  req.body;

        var title = trimString(dto.title);
		var length = parseInt(dto.length);
		var description = trimString(dto.description);
		var days = dto.days;
        
        var collection = db.get('programs');
       
        collection.insert({
            "title" : title,
			"description":description,
			"length":length,
			"days":days
        }, function (err, results) {
            if (err) {
                res.send("There was a problem with our database");
            }
            else {
				res.redirect("/addProgram");
            }
        });

    }
}

