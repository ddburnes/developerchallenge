
var express = require('express');
var routes = require('./routes');
var http = require('http');
var path = require('path');
var url = require('url');

var mongo = require('mongodb');
var monk = require('monk');
//var db = monk('readuser:reader1234@SG-mssmongodev02-874.servers.mongodirector.com:27017/dev-test');
var db = monk('localhost:27017/TurnerBroadcasting');

var app = express();

app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(express.cookieParser('your secret here'));
app.use(express.session());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'static')));

if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}
app.get('/', routes.index);
app.get('/programInfo', routes.programInfo(db,url));
app.get('/findProgram', routes.findProgram);
app.post('/findProgram', routes.findResults(db));
app.get('/addProgram', routes.addProgram(db));
app.post('/addProgram', routes.addResults(db));

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
